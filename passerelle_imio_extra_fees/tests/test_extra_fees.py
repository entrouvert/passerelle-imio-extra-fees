# DJANGO_SETTINGS_MODULE=passerelle.settings PASSERELLE_SETTINGS_FILE=tests/settings.py py.test --pdb

import django_webtest
import pytest
from django.contrib.contenttypes.models import ContentType
from passerelle.base.models import AccessRight, ApiUser

from passerelle_imio_extra_fees.models import ExtraFees


@pytest.fixture
def app(request):
    # creation de l'application Django
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@pytest.fixture
def connector(db):
    # creation du connecteur et ouverture de la permission "can_access" sans authentification.
    connector = ExtraFees.objects.create(slug='test')
    api = ApiUser.objects.create(username='all', keytype='', key='')
    obj_type = ContentType.objects.get_for_model(connector)
    AccessRight.objects.create(
        codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=connector.pk
    )
    return connector


def test_zero_fees_compute(app, connector):
    dic = {'data': []}
    resp = app.post_json('/extra-fees/test/compute', params=dic)
    assert resp.json.get('data')[0].get('amount') == '0.00'


def test_fees_compute(app, connector):
    connector.max_doc_in_letter = 5
    connector.belgium_postage_fee = '0.5'
    connector.save()
    nb_document = 18
    dic = {
        'data': [
            {'request_data': {'nb_documents': nb_document, 'postage_fee': connector.belgium_postage_fee}}
        ]
    }
    resp = app.post_json('/extra-fees/test/compute', params=dic)
    assert resp.json.get('data')[0].get('amount') == '2.00'
