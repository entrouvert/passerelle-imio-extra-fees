from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_imio_extra_fees', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extrafees',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
