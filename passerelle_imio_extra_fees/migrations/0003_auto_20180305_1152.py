import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_imio_extra_fees', '0002_auto_20170921_1213'),
    ]

    operations = [
        migrations.AddField(
            model_name='extrafees',
            name='belgium_postage_fee',
            field=models.DecimalField(
                default=0.0, verbose_name='Postage fees for belgium', max_digits=6, decimal_places=2
            ),
        ),
        migrations.AddField(
            model_name='extrafees',
            name='europe_postage_fee',
            field=models.DecimalField(
                default=0.0, verbose_name='Postage fees for europe', max_digits=6, decimal_places=2
            ),
        ),
        migrations.AddField(
            model_name='extrafees',
            name='max_doc_in_letter',
            field=models.PositiveSmallIntegerField(
                default=5,
                verbose_name='Maximum documents in one letter',
                validators=[
                    django.core.validators.MaxValueValidator(100),
                    django.core.validators.MinValueValidator(1),
                ],
            ),
        ),
        migrations.AddField(
            model_name='extrafees',
            name='profile',
            field=models.CharField(
                default=b'Default', max_length=30, choices=[(b'DEFAULT', b'Default'), (b'NAMUR', b'Namur')]
            ),
        ),
        migrations.AddField(
            model_name='extrafees',
            name='world_postage_fee',
            field=models.DecimalField(
                default=0.0, verbose_name='Postage fees for rest of the world', max_digits=6, decimal_places=2
            ),
        ),
        migrations.AlterField(
            model_name='extrafees',
            name='description',
            field=models.TextField(verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='extrafees',
            name='title',
            field=models.CharField(max_length=50, verbose_name='Title'),
        ),
    ]
