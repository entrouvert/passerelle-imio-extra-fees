#
# passerelle-imio-extra-fees - compute basket extra fees
# Copyright (C) 2017 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from decimal import Decimal

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.encoding import force_str
from django.utils.translation import gettext_lazy as _
from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint


class ExtraFees(BaseResource):
    category = _('Misc')
    PROFILE_CHOICES = (
        ('DEFAULT', 'Default'),
        ('NAMUR', 'Namur'),
    )

    profile = models.CharField(max_length=30, choices=PROFILE_CHOICES, default='Default')
    max_doc_in_letter = models.PositiveSmallIntegerField(
        verbose_name=_('Maximum documents in one letter'),
        default=5,
        validators=[MaxValueValidator(100), MinValueValidator(1)],
    )
    belgium_postage_fee = models.DecimalField(
        decimal_places=2, max_digits=6, verbose_name=_('Postage fees for belgium'), default=0.00
    )
    europe_postage_fee = models.DecimalField(
        decimal_places=2, max_digits=6, verbose_name=_('Postage fees for europe'), default=0.00
    )
    world_postage_fee = models.DecimalField(
        decimal_places=2, max_digits=6, verbose_name=_('Postage fees for rest of the world'), default=0.00
    )

    # free_postage_exeptions_list_doc ?

    class Meta:
        verbose_name = _('Extra Fees')

    @classmethod
    def get_connector_slug(cls):
        return 'extra-fees'

    # Minimum requirement in request to compute basket with postage fee
    # nb_documents : Number of desired documents for one citizen posted form.
    # postage_fee : THanks to citizen choice (Be/Eu/Wrld), Postage Fee value for this form.
    def default_compute(self, request, **kwargs):
        nb_documents = 0
        nb_letter = 0
        data = json.loads(request.body)
        postage_fee = Decimal(self.belgium_postage_fee)
        for basket_item in data['data']:
            try:
                nb_documents += int(basket_item['request_data']['nb_documents'])
                destination = basket_item['request_data']['destination']  # Passed in the workflow webservice
                #  No postage fees if send by mail
                if destination and (
                    'mail' in destination or 'commune' in destination or destination == 'False'
                ):
                    postage_fee = Decimal(0)
                if Decimal(basket_item['request_data']['postage_fee']) > postage_fee:
                    postage_fee = Decimal(basket_item['request_data']['postage_fee'])
            except KeyError:
                # basket item not associated with any document, no fee
                pass
        nb_letter = int(nb_documents / int(self.max_doc_in_letter)) + (
            ((nb_documents % int(self.max_doc_in_letter)) > 0) and 1 or 0
        )
        postage_fee = nb_letter * postage_fee
        return {'data': [{'subject': force_str(_('Postage')), 'amount': str(postage_fee)}]}

    def namur_compute(self, request, **kwargs):
        data = json.loads(request.body)
        # EXCEPTIONS :
        # duplicata-de-livret-de-mariage (frais port 8 ou 13)
        # demande-de-changement-d-adresse-domicile (frais port 0)
        changement_adresse_exception = False
        duplicata_exception = False
        duplicata_country_price = Decimal('0.00')
        duplicata_nb_doc = 0
        max_doc_in_letter = 5
        nb_documents = 0
        nb_letter = 0
        postage_fee = Decimal(self.belgium_postage_fee)
        # Balaie chaque elem du panier pour gerer les exceptions.
        for basket_item in data['data']:
            if 'demande-de-changement-d-adresse-domicile' in basket_item['request_data']['form_slug']:
                changement_adresse_exception = True
            if 'duplicata-de-livret-de-mariage' in basket_item['request_data']['form_slug']:
                duplicata_exception = True
                duplicata_country_price = Decimal(basket_item['request_data']['country_price'])
                duplicata_nb_doc = int(basket_item['request_data']['nb_documents'])
        for basket_item in data['data']:
            try:
                nb_documents += int(basket_item['request_data']['nb_documents'])
                if (
                    Decimal(basket_item['request_data']['country_price']) > postage_fee
                    and 'duplicata-de-livret-de-mariage' not in basket_item['request_data']['form_slug']
                ):
                    postage_fee = Decimal(basket_item['request_data']['country_price'])
            except KeyError:
                # basket item not associated with any document, no fee
                pass

        if changement_adresse_exception == True and nb_documents == 1:
            postage_fee = Decimal('0.00')
        else:
            if changement_adresse_exception == True and nb_documents > 1:
                nb_documents = nb_documents - 1
            nb_letter = int(nb_documents / max_doc_in_letter) + (
                ((nb_documents % max_doc_in_letter) > 0) and 1 or 0
            )
            postage_fee = nb_letter * postage_fee
            if duplicata_exception == True:
                postage_fee = (
                    duplicata_country_price if duplicata_country_price > postage_fee else postage_fee
                )
        return {'data': [{'subject': force_str(_('Postage')), 'amount': str(postage_fee)}]}

    @endpoint(methods=['post'])
    def compute(self, request, **kwargs):
        if self.profile == 'NAMUR':
            data = self.namur_compute(request, **kwargs)
        else:
            data = self.default_compute(request, **kwargs)
        return data

    # Namur : historical Namur datasource use id like fees value.
    # So, we need to keep 0.74 and 1.13 like id and use fee key to get real fee value.
    @endpoint()
    def namur_fees(self, request):
        return {
            'data': [
                {'id': '0.74', 'fee': self.belgium_postage_fee, 'text': 'En Belgique'},
                {'id': '1.13', 'fee': self.europe_postage_fee, 'text': 'À l\'étranger'},
            ]
        }

    # wcs : new webservice call : https://[COMMUNE]-passerelle.guichet-citoyen.be/extra-fees/[CONNECTOR-SLUG]/fees
    # datasources : webservice.fees.get('belgium')
    @endpoint()
    def fees(self, request):
        return {
            'belgium': self.belgium_postage_fee,
            'europe': self.europe_postage_fee,
            'world': self.world_postage_fee,
        }

    @endpoint()
    def destination_choices(self, request, q=None, **kwargs):
        destination_choices = {
            'data': [
                {'id': 'belgium', 'text': 'En belgique', 'fee': self.belgium_postage_fee},
                {'id': 'europe', 'text': 'En europe', 'fee': self.europe_postage_fee},
                {'id': 'world', 'text': 'Dans le reste du monde', 'value': self.world_postage_fee},
            ]
        }
        return destination_choices
